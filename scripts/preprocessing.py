import pandas as pd
import numpy as np
# from sklearn.preprocessing import MinMaxScaler


class DataProcessing:
    def __init__(self, file):
        self.file = pd.read_csv(file, parse_dates=['formatted_date'], index_col='formatted_date')

    def gen_train(self, train_rate, shifts_num):
        """
        Generates training data
        :param seq_len: length of window
        :return: X_train and Y_train
        """
        ticker = self.file.columns[0]
        train_len = round(self.file.shape[0]*train_rate)
        self.X_train = self.file.iloc[:train_len]
        self.Y_train = self.X_train.loc[:,ticker].shift(-1)
        for n in range(2, shifts_num+1):
            self.X_train['shift_'+str(n)] = None
            self.X_train.loc[:,'shift_'+str(n)] = self.X_train.loc[:,ticker].shift(n)
        self.X_train = self.X_train.drop(ticker,axis=1)
        return self.X_train, self.Y_train

    def gen_test(self, train_rate, shifts_num):
        """
        Generates test data
        :param seq_len: Length of window
        :return: X_test and Y_test
        """
        ticker = self.file.columns[0]
        train_len = round(self.file.shape[0]*train_rate)
        self.X_test = self.file.iloc[train_len:]
        self.Y_test = self.X_test.shift(-1)
        for n in range(2, shifts_num+1):
            self.X_test['shift_'+str(n)] = self.X_test[ticker].shift(n)
        self.X_test = self.X_test.drop(ticker,axis=1)
        return self.X_test, self.Y_test
    
#     def scale(self):
#         """
#         Generates scaled data
#         :return: scaled data
#         """
#         scaler = MinMaxScaler()
#         scaler.fit(self.Y_train)
#         self.X_tarin_scaled = scaler.transform(self.X_train)
#         self.Y_train_scaled = scaler.transform(self.Y_train)
#         self.X_test_scaled = scaler.transform(self.X_test)
#         self.Y_test_scaled = scaler.transform(self.Y_test)
        