from yahoofinancials import YahooFinancials
import pandas as pd


def get_stock_data(ticker, start_date, end_date, periodicity):
    """
    Gets historical stock data of given tickers between dates
    :param ticker: company, or companies whose data is to fetched
    :type ticker: string or list of strings
    :param start_date: starting date for stock prices
    :type start_date: string of date "YYYY-mm-dd"
    :param end_date: end date for stock prices
    :type end_date: string of date "YYYY-mm-dd"
    :param periodicity: type of period ("daily","weekly","monthly")
    :type periodicity: string 
    :return: stock_data.csv
    """
    i = 1
    try:
        all_data = YahooFinancials(ticker).get_historical_price_data(start_date, end_date, periodicity)
    except ValueError:
        print("ValueError, trying again")
        i += 1
        if i < 5:
            time.sleep(10)
            YahooFinancials(ticker).get_historical_price_data(start_date, end_date, periodicity)
        else:
            print("Tried 5 times, Yahoo error. Trying after 2 minutes")
            time.sleep(120)
            YahooFinancials(ticker).get_historical_price_data(start_date, end_date, periodicity)
    stock_data = pd.DataFrame(all_data[ticker]['prices']).set_index('formatted_date')
    stock_data = stock_data['close'].rename(ticker)
    stock_data.to_csv("datasets/{}_prices.csv".format(ticker))