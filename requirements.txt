matplotlib==3.5.1
numpy==1.22.3
pandas==1.4.2
scikit-learn==1.0.2
scipy==1.8.0
tensorflow-macos==2.8.0
yahoofinancials==1.6
